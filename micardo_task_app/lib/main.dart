import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:dio_proxy/dio_proxy.dart';
import 'dart:convert';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Micardo Task',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Micardo Task'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

Dio dio;

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController userNameTextEditingController;
  TextEditingController passwordTextEditingController;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Die IP ist:'),
            FutureBuilder(
              future: getIp(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                    return CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                    );

                  case ConnectionState.done:
                    if (!snapshot.hasData) {
                      return Text('Error: ${snapshot.error}');
                    } else {
                      return Text(json.decode(snapshot.data)['ip']);
                    }
                }
                return null;
              },
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: TextFormField(
                keyboardType: TextInputType.text,
                maxLength: 50,
                autofocus: false,
                controller: userNameTextEditingController,
                decoration: InputDecoration(
                  labelText: "Username",
                  isDense: true,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    borderSide: BorderSide(
                      style: BorderStyle.solid,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: TextFormField(
                keyboardType: TextInputType.text,
                maxLength: 50,
                autofocus: false,
                controller: passwordTextEditingController,
                decoration: InputDecoration(
                  labelText: "Password",
                  isDense: true,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    borderSide: BorderSide(
                      style: BorderStyle.solid,
                    ),
                  ),
                ),
              ),
            ),
            FlatButton(
                color: Colors.blue,
                onPressed: () {
                  makeQueryWithLoginData(
                      userName: userNameTextEditingController.text.trim(),
                      password: passwordTextEditingController.text.trim());
                },
                child: Text('Make query', style: TextStyle(color: Colors.white),))
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    userNameTextEditingController = TextEditingController();
    passwordTextEditingController = TextEditingController();

    dio = Dio()
      ..options.baseUrl = "http://ipconfig.io"
      ..httpClientAdapter =
      HttpProxyAdapter(ipAddr: '136.243.254.196', port: 80);
  }

  @override
  void dispose() {
    super.dispose();

    userNameTextEditingController.dispose();
    passwordTextEditingController.dispose();
  }
}

Future<String> getIp() async {
  try {
    Response<String> response = await dio.get('/json');
    print(response.data);
    return response.data;
  }catch(e){
    print(e);
  }
}

Future<Response> makeQueryWithLoginData(
    {String userName, String password}) async {
  try{
    Response response = await dio.get('/json', queryParameters:{'username':userName, 'password':password});
    print(response.data);
    return response;
  }catch (e){
    print(e);
  }
}

